package s59160561.informatics.buu.bookingcar

import android.content.Context
import android.databinding.DataBindingUtil
import android.graphics.Color
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.inputmethod.InputMethodManager
import kotlinx.android.synthetic.main.activity_main.*
import s59160561.informatics.buu.bookingcar.databinding.ActivityMainBinding
import java.lang.Exception

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    private val booking: ArrayList<Slot> = ArrayList()
    private var slot: Int = 0
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)

        for(x in 0..2){
            booking.add(x, Slot("","",""))
        }
        setVisibleIsTrue()
        binding.apply {
            slot1Btn.setOnClickListener {
                slot = 0;
                slot1Btn.setBackgroundColor(Color.GRAY)
                slot2Btn.setBackgroundResource(android.R.drawable.btn_default);
                slot3Btn.setBackgroundResource(android.R.drawable.btn_default);
                try {
                    showValue(slot);
                    setVisibleIsFalse();
                } catch (e: IndexOutOfBoundsException) {
                    setVisibleIsFalse()
                }

            }
            slot2Btn.setOnClickListener {
                slot2Btn.setBackgroundColor(Color.GRAY)
                slot1Btn.setBackgroundResource(android.R.drawable.btn_default);
                slot3Btn.setBackgroundResource(android.R.drawable.btn_default);
                slot = 1;
                try {
                    showValue(slot);
                    setVisibleIsFalse()
                } catch (e: IndexOutOfBoundsException) {
                    setVisibleIsFalse()
                }
            }
            slot3Btn.setOnClickListener {
                slot3Btn.setBackgroundColor(Color.GRAY)
                slot1Btn.setBackgroundResource(android.R.drawable.btn_default);
                slot2Btn.setBackgroundResource(android.R.drawable.btn_default);
                slot = 2;
                try {
                    showValue(slot);
                    setVisibleIsFalse()
                } catch (e: IndexOutOfBoundsException) {
                    setVisibleIsFalse()
                }

            }
            submitBtn.setOnClickListener {
                setValueBooking(it)
            }
            cancelBtn.setOnClickListener {
                setVisibleIsTrue()
            }
        }
    }

    private fun setVisibleIsTrue() {
        binding.apply {
            registrationnumber_text.visibility = View.GONE
            brand_text.visibility = View.GONE
            name_text.visibility = View.GONE
            submit_btn.visibility = View.GONE
            cancel_btn.visibility = View.GONE
            slot1Btn.setBackgroundResource(android.R.drawable.btn_default);
            slot2Btn.setBackgroundResource(android.R.drawable.btn_default);
            slot3Btn.setBackgroundResource(android.R.drawable.btn_default);
        }
        if (slot == 0) {
            binding.slot1Btn.setText("EMPTY")
            booking.removeAt(0)
            booking.add(0,Slot("","",""));
        } else if (slot == 1) {
            binding.slot2Btn.setText("EMPTY")
            booking.removeAt(1)
            booking.add(1,Slot("","",""));
        } else if (slot == 2) {
            binding.slot3Btn.setText("EMPTY")
            booking.removeAt(2)
            booking.add(2,Slot("","",""));
        }

    }

    private fun setVisibleIsFalse() {
        binding.apply {
            registrationnumber_text.visibility = View.VISIBLE
            brand_text.visibility = View.VISIBLE
            name_text.visibility = View.VISIBLE
            submit_btn.visibility = View.VISIBLE
            cancel_btn.visibility = View.VISIBLE
        }
    }

    private fun setValueBooking(view:View) {
        val registrationnumber = binding.registrationnumberText.text.toString()
        val brand = binding.brandText.text.toString()
        val name = binding.nameText.text.toString()
        booking.add(slot , Slot(registrationnumber, brand, name))
        if (slot == 0) {
            binding.slot1Btn.setText("FULL")
        } else if (slot == 1) {
            binding.slot2Btn.setText("FULL")
        } else {
            binding.slot3Btn.setText("FULL")
        }
        val inputMethodManager = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        inputMethodManager.hideSoftInputFromWindow(view.windowToken,0)
    }

    private fun showValue(slot: Int) {
        binding.registrationnumberText.setText(booking.get(slot).registrationNumber)
        binding.brandText.setText(booking.get(slot).brand)
        binding.nameText.setText(booking.get(slot).name)
    }
}
